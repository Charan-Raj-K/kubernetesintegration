variable "gcp_credentials" {
    type = string
    description = "GCP service account credentials"
}

variable "gcp_project_id" {
    type = string
    description = "terraform k8s project name"
}

variable "gcp_region" {
    type = string
    description = "cluster region"
}

variable "gke_cluster_name" {
    type = string
    description = "GKE CLuster name"
}

variable "gcp_zones" {
    type = list(string)
    description = "list of zones"
}

variable "gcp_network" {
    type = string
    description = "VPC network"
}

variable "gcp_subnet" {
    type = string
    description = "subnet name"
}

variable "gke_default_node_pool_name" {
    type = string
    description = "gke node pool name"
}

variable "gke_service_account_name" {
    type = string
    description = "gke service account name"
}